FROM node:latest AS build
WORKDIR /application
ENV VITE_BASE_API_URL="http://172.174.70.152/api"
COPY . .
RUN yarn
RUN yarn build
RUN ls


FROM nginx:alpine
EXPOSE 80 443
COPY --from=build application/dist/ /usr/share/nginx/html




import {useEffect, useState} from 'react'
import BookCreate from './components/BookCreate'
import Book from "./classes/Book"
import BookList from "./components/BookList";
import BookService from "./services/BookService";


function App() {
  const [books, setBooks] = useState<Book[]>([])
  const createBook =
      (title: string, description: string, rating: number) =>
          BookService.addOneBook({title, description, rating, id: undefined}).then(() =>
              BookService.getAllBooks().then(books => setBooks(books)))
  const deleteBookById =
      (id: string | undefined) =>
          BookService.deleteOneBook(id).then(() =>
              BookService.getAllBooks().then(books => setBooks(books)))
  const editBookById =
      (id: string | undefined, book:Book) =>
          BookService.editOneBook({...book, id}).then(() =>
              BookService.getAllBooks().then(books => setBooks(books)))


  useEffect(() => {
    const updateBooksList = async () => {
      BookService.getAllBooks().then(books => setBooks(books))
    }
    updateBooksList().then()
  }, [])


  return (
    <div className="app">
      <h1>Reading List - {books.length}</h1>
      <BookList bookList={books} onDeleteOneItem={deleteBookById} onEditOneItem={editBookById}/>
      <BookCreate onCreate={createBook}/>
    </div>
  )
}

export default App

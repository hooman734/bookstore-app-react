
class Book {
	id: string | undefined
	title: string | undefined
	description: string | undefined
	rating: number | undefined
}

export default Book
import axios from 'axios'
import Book from "../classes/Book";

/**
 * .env requirements:
 * VITE_BASE_API_URL
 */
const BASE_URI = `${import.meta.env.VITE_BASE_API_URL}`

async function retrieveAll() {
    return axios<{all_books: Book[]}>({
        method: 'get',
        url: `${BASE_URI}/api/v1/books`
    })
}

async function addOne(book: Book) {
    return axios<{added_book: Book}>({
        method: 'post',
        url: `${BASE_URI}/api/v1/book`,
        data: book
    })
}

async function editById(book: Book) {
    return axios<{edited_book: Book}>({
        method: 'put',
        url: `${BASE_URI}/api/v1/book/${book.id}`,
        data: book
    })
}

async function deleteById(id: string | undefined) {
    return axios<{edited_book: Book}>({
        method: 'delete',
        url: `${BASE_URI}/api/v1/book/${id}`,
    })
}


export { retrieveAll, addOne, editById, deleteById }
import Book from "../classes/Book";
import {addOne, deleteById, editById, retrieveAll} from "./API";

class BookService {
  private static books: Book[] = []

  private static async updateLocally(): Promise<void> {
    return new Promise((resolve, reject) => {
      retrieveAll()
          .then(resp => this.books = [...resp.data.all_books])
          .then(() => resolve())
          .catch(() => reject())
    })
  }

  public static async getAllBooks(): Promise<Book[]> {
    return new Promise((resolve, reject) => {
      this.updateLocally()
          .then(() => resolve(this.books))
          .catch(() => reject())
    })
  }

  public static async addOneBook(book: Book): Promise<void> {
    return new Promise((resolve, reject) => {
      addOne(book)
          .then(() => this.updateLocally()
              .then(() => resolve())
              .catch(() => reject()))
          .catch(() => reject())
    })
  }

  public static async editOneBook(book: Book): Promise<void> {
    return new Promise((resolve, reject) => {
      editById(book)
          .then(() => this.updateLocally()
              .then(() => resolve())
              .catch(() => reject()))
          .catch(() => reject())
    })
  }

  public static async deleteOneBook(id: string | undefined): Promise<void> {
    return new Promise((resolve, reject) => {
      deleteById(id)
          .then(() => this.updateLocally()
              .then(() => resolve())
              .catch(() => reject()))
          .catch(() => reject())
    })
  }
}

export default BookService
import Book from "../../classes/Book";
import {Fragment, useState} from "react";
import BookEdit from "../BookEdit";

interface BookShowProps {
	book: Book
	onDelete: (id: string | undefined) => void
	onEdit: (id: string | undefined, book: Book) => void
}
function BookShow({ book, onDelete, onEdit }: BookShowProps) {
	const [showEdit, setShowEdit] = useState(false)
	const imageSource = `https://picsum.photos/seed/${book.id}/400/250`
	let content = (
		<Fragment>
			<b>{book.title}</b>
			<div>{book.description}</div>
			<img alt="image of the book" src={imageSource}></img>
			<i>Rating: {book.rating}</i>
		</Fragment>
	)
	function handleOnEdit(id: string | undefined, book: Book) {
		onEdit(id, book)
		setShowEdit(false)
	}
	if(showEdit) {
		content = <BookEdit onEdit={handleOnEdit} book={book} />
	}
	return (
			<div className="book-show">
				{content}
				<div className="actions">
					<button className="edit" onClick={() => setShowEdit(!showEdit)}>E</button>
					<button className="delete" onClick={() => onDelete(book.id)}>X</button>
				</div>
			</div>
	)
}

export default BookShow
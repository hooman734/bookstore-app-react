import React, {Fragment, useState} from "react";
import Book from "../../classes/Book";
import {editById} from "../../services/API";

interface BookEditProps {
    book: Book
    onEdit: (id: string | undefined, book: Book) => void
}
function BookEdit({ book, onEdit }: BookEditProps) {
    const [editBook, setEditBook] = useState(book)
    function handleInputTitle(e: React.FormEvent<HTMLInputElement>) {
        setEditBook({...editBook, title: e.currentTarget.value})
    }

    function handleInputDescription(e: React.FormEvent<HTMLInputElement>) {
        setEditBook({...editBook, description: e.currentTarget.value})
    }

    function handleInputRating(e: React.FormEvent<HTMLInputElement>) {
        setEditBook({...editBook, rating: +e.currentTarget.value})
    }

    function handleSubmit(e: React.FormEvent<HTMLFormElement>) {
        e.preventDefault()
        onEdit(book.id, editBook)
    }

    return (
        <Fragment>
            <form className="book-edit" onSubmit={handleSubmit}>
                <label>Title:</label>
                <input className="input" onInput={handleInputTitle} value={editBook.title}/>
                <label>Description:</label>
                <input className="input" onInput={handleInputDescription} value={editBook.description}/>
                <label>Rating:</label>
                <input className="input" onInput={handleInputRating} value={editBook.rating}/>
                <button className="button is-primary">Save</button>
            </form>
        </Fragment>

    )
}

export default BookEdit
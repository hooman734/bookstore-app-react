import React from "react";

interface BookCreateProps {
    onCreate: (name: string, description: string, rating: number) => Promise<void>
}
function BookCreate({ onCreate }: BookCreateProps) {

    const { useState } = React
    const [toggle, setToggle] = useState(false)
    const [title, setTitle] = useState('')
    const [description, setDescription] = useState('')
    const [rating, setRating] = useState('')
    function handleInputTitle(e: React.FormEvent<HTMLInputElement>) {
        setTitle(e.currentTarget.value)
    }

    function handleInputDescription(e: React.FormEvent<HTMLInputElement>) {
      setDescription(e.currentTarget.value)
    }

    function handleInputRating(e: React.FormEvent<HTMLInputElement>) {
      setRating(e.currentTarget.value)
    }

    function handleSubmit(e: React.FormEvent<HTMLFormElement>) {
        e.preventDefault()
        onCreate(title, description, +rating).then()
        setTitle('')
        setDescription('')
        setRating('')
    }

    const contentInput = <form onSubmit={handleSubmit}>
        <label>Input Book's Title</label>
        <input className="input" onInput={handleInputTitle} value={title} />
        <label>Input Book's Description</label>
        <input className="input" onInput={handleInputDescription} value={description}/>
        <label>Input Book's Rating</label>
        <input className="input" onInput={handleInputRating} value={rating} type="number" min="1" max="100"/>
        <button className="button">Create!</button>
    </form>

    const toggleUp = <button className="toggle-up" onClick={() => setToggle(!toggle)}>Toggle</button>

    const toggleDown = <button className="toggle-down" onClick={() => setToggle(!toggle)}>Toggle</button>

    return (
      <div className="book-create">
          {toggle && toggleDown}
          {!toggle && toggleUp}
          <h3>Add a Book</h3>
          {toggle && contentInput}
      </div>
    )
}

export default BookCreate